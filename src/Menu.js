import { BrowserRouter, Route, Switch } from "react-router-dom";
import App from './App'
import About from "./page/About";
import Areamonitor from "./page/Areamonitor";
import Ourteam from "./page/Ourteam";

function Menu() {
  return (
      <BrowserRouter>
        <Switch>
            <Route exact path='/' component={App} />
            <Route exact path='/showdata' component={Areamonitor} />
            <Route exact path='/about' component={About} />
            <Route exact path='/ourteam' component={Ourteam} />
        </Switch>
      </BrowserRouter>
  );
}

export default Menu;