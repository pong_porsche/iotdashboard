import React from 'react';
import background from "../img/wall3.png";
import Footer from '../component/Footer';
import Header from '../component/Header';
import './About.css';

function About() {
    return (
        <div style={{ backgroundImage: `url(${background})` }}>
            <Header/>
            <div className='container3'>
                <h1 style={{textAlign:'center', padding:20, fontWeight:'bold'}}>About This Project</h1>

                <div class="ui vertical stripe segment">
                <div class="ui text container"><p/>
                    <h4 class="ui">
                        This project develops an Internet of Thing (IoT) based radiation detection system (Radiation Area Monitoring) which data can be sent to 
                        web server via Wi-Fi and IoT system for monitoring radiation safety and security.
                    </h4>
                    <br/>
                </div>
            <br/><br/>

                <section className='about-con'>
                    <div className='about-l1'>
                        <img src='pic2.png' alt='iot' />
                    </div>
                    <div className='about-r1'>
                        <h3>We involve IoT technology with Radiation Dectection System.</h3>
                    </div>
                </section>
                <hr style={{margin:25}} />

                <section className='about-con'>
                    <div className='about-r2'>
                        <h3>With IoT, we can collect data from onsite monitor and decrease error that cause on human error.</h3>
                    </div>
                    <div className='about-l2'>
                        <img src='usergroup1.png' alt='people' />
                    </div>
                </section>

                <h1 style={{textAlign:'center', padding:20}}>Final Product</h1>

                <section className='about-con'>
                    <div className='about-r3'>
                        <img className="rounded" style={{height:285, width:400 }} src='Front.jpg' alt='front' />
                    </div>
                    <div className='about-l3'>
                        <img className="rounded" style={{height:285, width:400 }} src='Back.jpg' alt='back' />
                    </div>
                </section>

            </div>
            </div>
            <Footer/>
        </div>
    );
  }
  
  export default About;