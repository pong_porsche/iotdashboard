import React from 'react';
import Footer from '../component/Footer';
import Header from '../component/Header';
import AverageData from '../DBData/AverageData';
import CardData from '../DBData/CardData';
import ChartData from '../DBData/ChartData';
import LastUpdate from '../DBData/LastUpdate';
import Meter from '../DBData/Meter';
import MeterXL from '../DBData/MeterXL';
import TotalData from '../DBData/TotalData';
import './Areamonitor.css';

function Areamonitor() {
    return (
        <div>
            <Header/>
            <div className='container1'>
                <h1 style={{textAlign:'center', padding:20, fontWeight:'bold'}}>Data DashBoard</h1>
                <section className='area-con'>
                    <div className='area-l'>
                        <div className='AdjMeter'>
                            <Meter/>
                        </div> 
                        <div className='AdjMeterXL'>
                            <MeterXL/>
                        </div> 
                    </div>
                    <div className='area-r'>
                        <CardData/>
                    </div>
                </section>
            <hr style={{margin:25}} />

            <div className='AdjBox'>
                <div className='row'>
                    <div className='col'>
                        <div className='AdjCol'>
                            <LastUpdate/>
                        </div>
                    </div>
                    <div className='col'>
                        <div className='AdjCol'>
                            <TotalData/>
                        </div>
                    </div>
                    <div className='col'>
                        <div className='AdjCol'>
                            <AverageData/>
                        </div>
                    </div>
                </div>
            </div>
            <div className='AdjChart'>
                <ChartData/>
            </div>
            </div>
            <Footer/>
        </div>
    );
  }
  
  export default Areamonitor;