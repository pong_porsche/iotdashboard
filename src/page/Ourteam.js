import React from 'react';
import { Container } from "semantic-ui-react";
import background from "../img/wall2.png";
import Header from '../component/Header';
import Footer from '../component/Footer';
import './Ourteam.css';

const Ourteam = () => {
    return(
        <div style={{ backgroundImage: `url(${background})` }}>
            <Header/>
            <Container>
            <div className='container2'>
                <h1 style={{textAlign:'center', padding:20, fontWeight:'bold'}}>Our Team</h1>
                <section className='ourteam-con'>
                    <div className='ourteam-l'>
                        <img src='Pong.jpg' alt='pong' />
                    </div>
                    <div className='ourteam-r'>
                        <h3>Sarach Charoenthongkul</h3>
                        <h5>Contact: 6131116021@student.chula.ac.th</h5>
                        <h5>Faculty of Engineering, Chulalongkorn University</h5>
                    </div>
                </section>
                <hr style={{margin:25}} />

                <section className='ourteam-con'>
                    <div className='ourteam-r'>
                    <h3>Pichayanant Horavit</h3>
                        <h5>Contact: 6131111921@student.chula.ac.th</h5>
                        <h5>Faculty of Engineering, Chulalongkorn University</h5>
                    </div>
                    <div className='ourteam-l'>
                        <img src='Faii.jpg' alt='faii' />
                    </div>
                </section>
                <hr style={{margin:25}} />

                <section className='ourteam-con'>
                    <div className='ourteam-l'>
                        <img src='Mew.jpg' alt='mew' />
                    </div>
                    <div className='ourteam-r'>
                    <h3>Natthaphat Panpheng</h3>
                        <h5>Contact: 6131104521@student.chula.ac.th</h5>
                        <h5>Faculty of Engineering, Chulalongkorn University</h5>
                    </div>
                </section>
            </div>
                
            </Container>
            <Footer/>
        </div>
    )
}
export default Ourteam;