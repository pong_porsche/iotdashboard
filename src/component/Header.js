import React, {useState} from 'react';
import { FiMenu, FiX } from "react-icons/fi";
import { FaRadiation } from "react-icons/fa";
import { Link } from 'react-router-dom';
import './Header.css';

function Header() {

    const [click, setClick] = useState(false);
    const handleClick = () => setClick(!click);
    const closeMobileMenu = () => setClick(false);

    return(
        <div className='header'>
            <div className='container7'>
                <div className='header-con'>
                    <div className='logo-container'>
                        <Link to='/'>
                            <a href='/'>NucMatIoT <FaRadiation/></a>
                        </Link>
                    </div>
                    <ul className={click ?  'menu active' : 'menu'}>
                        <li className='menu-link' onClick={closeMobileMenu}>
                            <Link to='/showdata'>
                                <a href='/'>Area Monitoring</a>
                            </Link>
                        </li>
                        <li className='menu-link' onClick={closeMobileMenu}>
                            <Link to='/about'>
                                <a href='/'>About</a>
                            </Link>
                        </li>
                        <li className='menu-link' onClick={closeMobileMenu}>
                            <Link to='/ourteam'>
                                <a href='/'>OurTeam</a>
                            </Link>
                        </li>
                    </ul>
                    <div className='mobile-menu' onClick={handleClick}>
                        {click ? (
                            <FiX/>
                        ) : (
                            <FiMenu/>
                        )}
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Header;