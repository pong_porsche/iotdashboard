import React from 'react';
import { Link } from 'react-router-dom';
import './Banner.css';

let bannerData={
    title: 'IoT Radiation Area Monitoring',
    desc: 'FROM CONNECTION TO BENEFIT'
}

function Banner() {

    return(
        <div className='banner-bg'>
            <div className='overlay1'/>
            <div className='container4'>
                <div className='banner-con'>
                    <div className='banner-text'>
                        <h1>{bannerData.title}</h1>
                        <p>{bannerData.desc}</p>
                        <Link to='/showdata'>
                            <a href='/' className='banner-btn'>Learn More</a>
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Banner;