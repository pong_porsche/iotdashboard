import React from 'react';
import './Content.css';

function Content() {

    return(
        <div className='container6'>

            <div className="ui vertical stripe segment">
                <div className="ui middle aligned stackable grid container"><br/>
                    <div className="row">
                        <div className="eight wide column"><p/>
                            <h2 className="ui">What is IoT?</h2>
                            <p>The Internet of Things (IoT) describes the network of physical objects—“things”—that are embedded with 
                                sensors, software, and other technologies for the purpose of connecting and exchanging data with other 
                                devices and systems over the internet.
                            </p>
                            <h2 className="ui">Why is Internet of Things (IoT) so important?</h2>
                            <p>Over the past few years, IoT has become one of the most important technologies of the 21st century. 
                                Now that we can connect everyday objects—kitchen appliances, cars, thermostats, baby monitors—to 
                                the internet via embedded devices, seamless communication is possible between people, processes, 
                                and things.
                            </p>
                            <p/>
                        </div>
                        <div className="six wide right floated column">
                            <img style={{height:300, width:350}} alt='iot' src="IoTConnect1.png" class="ui large image"/>
                        </div>
                    </div>
                </div>
            </div>

            <div className="ui vertical stripe quote segment">
                <div className="ui equal width stackable internally celled grid">
                    <div className="center aligned row">
                        <div className="column">
                            <i className="heartbeat icon" style={{fontSize:35, color:'#000'}}></i>
                            <h1 style={{color:'#000'}}>"Society"</h1>
                            <p>Monitoring for health and social care.</p>
                        </div>
                        <div className="column">
                            <i className="wifi icon" style={{fontSize:35, color:'#000'}}></i>
                            <h1 style={{color:'#000'}}>"Productivity"</h1>
                            <p>
                            Identify and eliminate process errors.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div className="ui vertical stripe segment">
                <div className="ui text container"><p/>
                    <h2 className="ui">What is Radiation Area Monitoring?</h2>
                    <p>Area monitoring is used for the detection of X-ray or gamma radiation in a selected area. A monitor should 
                        be used in any area location where personnel may be exposed to an adverse amount of radiation.</p><p/>
                    <h4 className="ui horizontal divider">
                        <a href="https://hps.org/publicinformation/ate/faqs/radiationdetection.html">More about Radiation Detection</a>
                    </h4>
                    <p/>
                    <h2 className="ui">People exposed to less hazardous environments</h2>
                    <p>For more safety and security. We involve IoT technology with Radiation Dectection System.</p>
                    <p/>
                    <br/>
                </div>
            </div>

        </div>
    )
}
export default Content;