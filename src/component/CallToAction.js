import React from 'react';
import './CallToAction.css';
import { FiCpu } from 'react-icons/fi';

function CallToAction() {

    return(
        <div className='cta-bg'>
            <div className='overlay'></div>
            <div className='container5'>
                <div className='cta-text'>
                    <FiCpu className='FiCode'/>
                    <h2>Nuclear Instrument Laboratory</h2>
                    <p>Department of Nuclear Engineering
                        Faculty of Engineering, Chulalongkorn University</p>
                    <a href='http://www.ne.eng.chula.ac.th/wp/' className='cta-btn'>get to know us</a>
                </div>
            </div>
        </div>
    )
}
export default CallToAction;