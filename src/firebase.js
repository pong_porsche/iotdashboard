import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore/lite';

const firebaseConfig = {
    apiKey: "AIzaSyDEJN4B6quUS_uf9ekLgYhPJeubFHHNfTU",
    authDomain: "seniorproject-560ce.firebaseapp.com",
    projectId: "seniorproject-560ce",
    storageBucket: "seniorproject-560ce.appspot.com",
    messagingSenderId: "847674146053",
    appId: "1:847674146053:web:bd3359498618b0cc084b6c",
    measurementId: "G-56JEB0QP0J"
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

export default db;