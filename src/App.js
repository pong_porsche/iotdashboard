import Banner from './component/Banner';
import CallToAction from './component/CallToAction';
import Content from './component/Content';
import Footer from './component/Footer';
import Header from './component/Header';

function App() {
  return (
    <>
        <Header/>
        <Banner/>
        <Content/>
        <CallToAction/>
        <Footer/>
    </>
  );
}

export default App;
