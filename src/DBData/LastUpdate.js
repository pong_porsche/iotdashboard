import { Component } from "react";
import { collection, getDocs } from 'firebase/firestore/lite';
import db from '../firebase';

class LastUpdate extends Component {

    constructor(props){
        super(props);
        this.state = {
            }
    }

    componentDidMount(){
        this.queryFirebase();
    }

    async queryFirebase(){
        const getcount = collection(db, 'autoupload-test');
        const countSnapshot = await getDocs(getcount);
        countSnapshot.docs.map(doc => this.setState({
            [doc.id] : {
                Date: doc.data().date,
                Time: doc.data().time,
                Count: doc.data().count,
            }
        }))
    }

    LastDate(){

        let lastdate=[];    

        Object.values(this.state)
        .sort((x, y) => {
            let xDate = parseInt(x.Date.split('/').join(''));
            let xTime = parseInt(x.Time.split(':').join(''));
            let yDate = parseInt(y.Date.split('/').join(''));
            let yTime = parseInt(y.Time.split(':').join(''));
            if (xDate > yDate){
                return 1;
            }
            else if (xDate < yDate){
                return -1;
            }
            else{
                return (xTime > yTime) ? 1 : -1;
            }
        })
        .forEach(data => {
            lastdate.push(data.Date)
        })
        return lastdate[lastdate.length-1];
    }

    LastTime(){

        let lasttime=[];

        Object.values(this.state)
        .sort((x, y) => {
            let xDate = parseInt(x.Date.split('/').join(''));
            let xTime = parseInt(x.Time.split(':').join(''));
            let yDate = parseInt(y.Date.split('/').join(''));
            let yTime = parseInt(y.Time.split(':').join(''));
            if (xDate > yDate){
                return 1;
            }
            else if (xDate < yDate){
                return -1;
            }
            else{
                return (xTime > yTime) ? 1 : -1;
            }
        })
        .forEach(data => {
            lasttime.push(data.Time)
        })
        return lasttime[lasttime.length-1];
    }

    render(){
        return(
            <div>
                <div className="ui cards">
                    <div className="card">
                        <div className="content">
                        <h2>Last Update</h2>
                        <div className="description ">
                            <h5>Date : {this.LastDate()}</h5>
                            <h5>Time : {this.LastTime()}</h5>
                        </div>
                        </div>
                    </div>

                </div>   
            </div>
        )
    }
}
export default LastUpdate;