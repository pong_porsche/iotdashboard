import { Component } from "react";
import { collection, getDocs } from 'firebase/firestore/lite';
import db from '../firebase';

class TotalData extends Component {

    constructor(props){
        super(props);
        this.state = {
            }
    }

    componentDidMount(){
        this.queryFirebase();
    }

    async queryFirebase(){
        const getcount = collection(db, 'autoupload-test');
        const countSnapshot = await getDocs(getcount);
        countSnapshot.docs.map(doc => this.setState({
            [doc.id] : {
                Date: doc.data().date,
                Time: doc.data().time,
                Count: doc.data().count,
            }
        }))
    }

    totaldata(){

        let index=[];

        Object.values(this.state)
        .forEach(data => {
            index.push(data)
        })
        return index.length;
    }


    render(){
        return(
            <div>
                <div className="ui cards">
                    <div className="card">
                        <div className="content">
                        <h2>Total Data</h2>
                        <div className="description ">
                            <h1>
                                # {this.totaldata()}
                            </h1>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default TotalData;