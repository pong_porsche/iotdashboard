import React, {Component} from 'react';
import { collection, getDocs } from 'firebase/firestore/lite';
import db from '../firebase';
import Chart from "react-google-charts";

class ChartData extends Component {

    constructor(props){
        super(props);
        this.state = {};
    }
    componentDidMount(){
        this.queryFirebase();
    }

    async queryFirebase(){
        const getcount = collection(db, 'autoupload-test');
        const countSnapshot = await getDocs(getcount);
        countSnapshot.docs.map(doc => this.setState({
            [doc.id] : {
                Date: doc.data().date,
                Time: doc.data().time,
                Count: doc.data().count,
            }
        }))
    }

    countstable(){
        var dataArray = [["Time", "CPS"]];
        
        Object.values(this.state)
        .sort((x, y) => {
            let xDate = parseInt(x.Date.split('/').join(''));
            let xTime = parseInt(x.Time.split(':').join(''));
            let yDate = parseInt(y.Date.split('/').join(''));
            let yTime = parseInt(y.Time.split(':').join(''));
            if (xDate > yDate){
                return 1;
            }
            else if (xDate < yDate){
                return -1;
            }
            else{
                return (xTime > yTime) ? 1 : -1;
            }
        })
        .forEach((data, index) => {
            dataArray.push(['#'+(index+1), data.Count]) 
            if (dataArray.length === 12){
                dataArray.splice(1,1);
            }
        })
        return dataArray;
    }
    
    countdata(){
        return (
            
            <div style={{ display: 'flex', maxWidth: 900 }}>
                <Chart
                    width={800}
                    height={'350px'}
                    chartType="AreaChart"
                    loader={<div>Loading Chart</div>}
                    data={this.countstable()}
                    options={{
                        pointSize: 6,
                        hAxis: { title: '10 latest data'},
                        vAxis: { minValue: 0 , title: 'Counts'},
                        chartArea: { width: '75%', height: '60%' },
                    }}
                />
            </div>
        )
    }
    render (){
        return( 
            <div>
                {this.countdata()}
            </div>
        )
    }
}
export default ChartData;