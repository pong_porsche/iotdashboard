import ReactSpeedometer from "react-d3-speedometer";
import { Component } from "react";
import { collection, getDocs } from 'firebase/firestore/lite';
import db from '../firebase';

class Meter extends Component {

    constructor(props){
        super(props);
        this.state = {
            }
    }

    componentDidMount(){
        this.queryFirebase();
    }

    async queryFirebase(){
        const getcount = collection(db, 'autoupload-test');
        const countSnapshot = await getDocs(getcount);
        countSnapshot.docs.map(doc => this.setState({
            [doc.id] : {
                Date: doc.data().date,
                Time: doc.data().time,
                Count: doc.data().count,
            }
        }))
    }

    countdata(){
        let lastdata=[];
        Object.values(this.state)
        .sort((x, y) => {
            let xDate = parseInt(x.Date.split('/').join(''));
            let xTime = parseInt(x.Time.split(':').join(''));
            let yDate = parseInt(y.Date.split('/').join(''));
            let yTime = parseInt(y.Time.split(':').join(''));
            if (xDate > yDate){
                return 1;
            }
            else if (xDate < yDate){
                return -1;
            }
            else{
                return (xTime > yTime) ? 1 : -1;
            }
        })
        .forEach(data => {
            lastdata.push(data.Count)
        })
        return lastdata[lastdata.length-1];
    }


    render(){
        
        let metertext = this.countdata() + ' CPS';
        return(
                <ReactSpeedometer 
                    width={350}
                    value={this.countdata()}
                    currentValueText = {metertext}
                    maxSegmentLabels={5}
                    maxValue={10000}
                    segments={5000}
                    startColor="lightgreen"
                    endColor="red"
                    ringWidth={20}
                />
        )
    }
}
export default Meter;