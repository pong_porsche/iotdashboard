import {Component} from 'react';
import { collection, getDocs } from 'firebase/firestore/lite';
import db from '../firebase';

class GetData extends Component {
    constructor(props) {
        super(props);
        this.state={

        }
    }

    async componentDidMount(){
        const getcount = collection(db, 'autoupload-test');
        const countSnapshot = await getDocs(getcount);
        countSnapshot.docs.map(doc => this.setState({
            [doc.id] : {
                Date: doc.data().date,
                Time: doc.data().time,
                Count: doc.data().count,
            }
        }))
    }

    showdata(){
        return Object.values(this.state).sort((x,y) => {
            let xDate = parseInt(x.Date.split('/').join(''));
            let yDate = parseInt(y.Date.split('/').join(''));
            let xTime = parseInt(x.Time.split(':').join(''));
            let yTime = parseInt(y.Time.split(':').join(''));

            if (xDate < yDate){
                return -1;
            }
            else if(xDate > yDate) {
                return 1;
            }
            else{
                return (xTime > yTime) ? 1:-1;
            }
        })
        .map(data => (
            <div className='row' style={{color:'#5F5E5E'}}>
                <div className='col text-center' ><h6>{data.Date}</h6></div>
                <div className='col text-center'><h6>{data.Time}</h6></div>
                <div className='col text-center'><h6>{data.Count}</h6></div>
            </div>
        ))
    }

    render(){
        return(
            <div>
                {this.showdata()}
            </div>
        )
    }
}
export default GetData;