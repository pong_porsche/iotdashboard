import { Component } from "react";
import { collection, getDocs } from 'firebase/firestore/lite';
import db from '../firebase';

class AverageData extends Component {

    constructor(props){
        super(props);
        this.state = {
            }
    }

    componentDidMount(){
        this.queryFirebase();
    }

    async queryFirebase(){
        const getcount = collection(db, 'autoupload-test');
        const countSnapshot = await getDocs(getcount);
        countSnapshot.docs.map(doc => this.setState({
            [doc.id] : {
                Date: doc.data().date,
                Time: doc.data().time,
                Count: doc.data().count,
            }
        }))
    }

    avgdata(){

        let sum=0;
        let index=0;

        Object.values(this.state)
        .forEach(data => {
            sum += data.Count;
            index++;
        })
        return sum/index;
    }


    render(){
        return(
            <div class="ui cards">
                <div class="card">
                    <div class="content">
                        <h2>Average Counts</h2>
                        <div class="description">
                            <h1>{this.avgdata().toFixed(2)}</h1>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default AverageData;