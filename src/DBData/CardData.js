import GetData from "./GetData";
import './CardData.css';

const CardData = () => {
    return(
        <div className='countscard'>
            <h4 className='cardtext'>Recent Data</h4>
            <div className='row text-center'>
                <div className='col'><h5>Date</h5></div>
                <div className='col'><h5>Time</h5></div>
                <div className='col'><h5>Counts</h5></div>
            </div><br/>
            <div className='countsscrollbox'><GetData/></div>
            
        </div>
    )
}
export default CardData;